//
// Created by samjjx on 2019/4/30.
//

#ifndef CPP_READER_HPP
#define CPP_READER_HPP

#include <iostream>
#include <iostream> // for std::cout
#include <utility> // for std::pair
#include <algorithm> // for std::for_each


class Reader {
public:
    Reader(){}
    ~Reader(){}

public:
    void Read();
};

#endif //CPP_READER_HPP
