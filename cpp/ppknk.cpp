#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <utility>
#include <algorithm>
#include <ctime>
#include <random>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/page_rank.hpp>

#include <glog/logging.h>
#include "io/Reader.hpp"


using namespace boost;



struct VertexP {
    // distance map: (w,d)
    std::unordered_map<int, int> pads;
    std::unordered_set<int> klist;
    int vid;
};

struct EdgeWeight {
    int weight;
};

typedef adjacency_list<vecS, vecS, directedS, VertexP, property<edge_weight_t, EdgeWeight>> Graph;
typedef property_map<Graph, vertex_index_t>::type IndexMap;
typedef iterator_property_map<std::vector<double>::iterator, IndexMap> RankMap;
typedef graph_traits<Graph>::vertex_iterator VIter;
typedef graph_traits<Graph>::out_edge_iterator EIter;
typedef graph_traits<Graph>::edge_descriptor edge_descriptor;
typedef std::pair<int, int> Edge;
typedef graph_traits<Graph>::vertex_descriptor VD;

struct KPADS {
    std::unordered_map<int, std::unordered_map<int, int>> kpads;
};

struct KNK {
    VD root;
    // For completion
    VD target;
    int keyword;
    int dist;
};



using namespace std;

int k = 1;
KPADS kpads;


std::unordered_map<int, VD> nodes_pub;
std::unordered_map<int, VD> nodes_pri;
std::unordered_map<int, std::unordered_map<int, int>> ads_labels;
std::unordered_map<int, std::unordered_set<VD>> key2vertices_pub;
std::unordered_map<int, std::unordered_set<VD>> key2vertices_pri;
//
//int** pub_dist;
//int** pri_dist;

std::vector<int> dijkstra(Graph &g, std::unordered_map<int, VD> &nodes, int source, int R = 2) {
  priority_queue<pair<int, int>> heap;
  std::unordered_set<int> visited;
  std::vector<int> dist(num_vertices(g));

  for (int i = 0; i < nodes.size(); i++) {
    dist[i] = std::numeric_limits<int>::max();
  }

  dist[source] = 0;
  heap.push(make_pair(0, source));

  while (!heap.empty()) {
    int u = heap.top().second;
    int dis = -heap.top().first;
    if (dis >= R)
      return dist;
    heap.pop();

    if (visited.find(u) != visited.end()) {
      continue;
    }

    visited.insert(u);

    EIter i, end;

    for (tie(i, end) = out_edges(nodes[u], g); i != end; i++) {
      auto v = target(*i, g);
      auto len = get(edge_weight_t(), g, *i).weight;

      if (dist[v] > dist[u] + len) {
        dist[v] = dist[u] + len;
        heap.push(make_pair(-dist[v], v));
      }
    }
  }
  return dist;
}


/*
 * The portal distance refinement
 */
void PDU(Graph g_pub, Graph g_pri, std::unordered_map<int, int> portals,
         std::unordered_map<int, std::unordered_map<int, int>> &dist_pub,
         std::unordered_map<int, std::unordered_map<int, int>> &dist_pri,
         std::unordered_map<int, std::unordered_map<int, int>> &changes) {
  LOG(INFO) << "COMPUTE PDU!!!!!!!";
  for (auto p: portals) {
    std::unordered_map<int, int> m_pri, m_pub;
    dist_pri[p.first] = m_pri;
    dist_pub[p.second] = m_pub;
  }
  LOG(INFO) << "PDU INIT!!!!!!!";
  LOG(INFO) << "Portal Size!!!!!!!" << dist_pri.size();

  priority_queue<pair<int, pair<int, int>>> heap;
  int cnt = 0;
  for (auto p1: dist_pri) {
    std::vector<int> d_pri = dijkstra(g_pri, nodes_pri, p1.first);
    std::vector<int> d_pub = dijkstra(g_pub, nodes_pub, portals[p1.first]);

    for (auto p2: dist_pri) {
      if (p1.first == p2.first) continue;
      if (d_pri[p2.first] == std::numeric_limits<int>::max() &&
          d_pub[portals[p2.first]] == std::numeric_limits<int>::max())
        continue;
      if (d_pri[p2.first] == std::numeric_limits<int>::max()) {
        dist_pri[p1.first][p2.first] = d_pub[portals[p2.first]];
        heap.push(make_pair(-dist_pri[p1.first][p2.first], make_pair(p1.first, p2.first)));
        continue;
      }

      if (d_pri[p2.first] > d_pub[portals[p2.first]]) {
        dist_pri[p1.first][p2.first] = d_pub[portals[p2.first]];
        heap.push(make_pair(-dist_pri[p1.first][p2.first], make_pair(p1.first, p2.first)));
      } else {
        dist_pri[p1.first][p2.first] = d_pri[p2.first];
      }
    }
    cnt++;
    if (cnt % 10 == 0) {
      LOG(INFO) << cnt << "\tout of" << dist_pri.size();
      LOG(INFO) << "# of heap\t" << heap.size();
    }
  }

  std::clock_t start;
  double duration;

  start = std::clock();

  LOG(INFO) << "Change size\t" << heap.size();
  int cnt_change = 0;
  while (!heap.empty()) {
    int dist = -heap.top().first;
    int p1 = heap.top().second.first;
    int p2 = heap.top().second.second;

    changes[p1][p2] = dist;
    cnt_change++;

    heap.pop();

    for (auto pi: portals) {
      if (dist_pri[pi.first].find(p1) != dist_pri[pi.first].end()) {
        int nd = dist_pri[pi.first][p1] + dist;
        if (dist_pri[pi.first].find(p2) == dist_pri[pi.first].end() ||
            nd < dist_pri[pi.first][p2]) {
          dist_pri[pi.first][p2] = nd;
          heap.push(make_pair(-nd, make_pair(pi.first, p2)));
        }
      }

      if (dist_pri[pi.first].find(p2) != dist_pri[pi.first].end()) {
        int nd = dist_pri[pi.first][p2] + dist;
        if (dist_pri[pi.first].find(p1) == dist_pri[pi.first].end() ||
            nd < dist_pri[pi.first][p1]) {
          dist_pri[pi.first][p1] = nd;
          heap.push(make_pair(-nd, make_pair(pi.first, p1)));
        }
      }
    }
  }
  LOG(INFO) << "Portal changes size\t" << cnt_change;
  duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
  LOG(INFO) << "PDU REFINE time \t" << duration;
}

void loadADS(std::string adspath, Graph &g, std::unordered_map<int, VD> &nodes) {
  ifstream adsfile(adspath);
  std::string line;
  while (std::getline(adsfile, line)) {
    std::istringstream s(line);
    int vid, w, d;
    s >> vid;
    std::unordered_map<int, int> tmp;

    while ((s >> w >> d)) {
      tmp[w - 1] = d;
    }
    g[nodes[vid - 1]].pads = tmp;
  }
  /*
   * Generate KPADS by PADS
   */
  for (auto kv: key2vertices_pub) {
    auto keyword = kv.first;
    if (kpads.kpads.find(keyword) == kpads.kpads.end()) {
      std::unordered_map<int, int> ads;
      kpads.kpads.insert(std::make_pair(keyword, ads));
    }
    auto VDs = kv.second;

    std::unordered_map<int, int> toMerge;
    for (auto v: VDs) {
      for (auto wd: g[v].pads) {
        auto w = wd.first;
        auto d = wd.second;
        if (toMerge.find(w) == toMerge.end())
          toMerge.insert(std::make_pair(w, d));
        else {
          toMerge[w] = toMerge[w] > d ? d : toMerge[w];
        }
        /* Merge the common centers
        if (kpads.kpads[keyword].find(w) == kpads.kpads[keyword].end()) {
          kpads.kpads[keyword].insert(std::make_pair(w, d));
        } else {
          if (kpads.kpads[keyword][w] > d)
            kpads.kpads[keyword][w] = d;
        }
        */
      }
    }
    for (auto wd: toMerge) {
      auto w = wd.first;
      auto d = wd.second;
      kpads.kpads[keyword].insert(std::make_pair(w, d));
    }
  }
}

bool contain_keyword_pri(int t) {
  return key2vertices_pri.find(t) != key2vertices_pri.end();
}

bool contain_keyword_pub(int t) {
  return key2vertices_pub.find(t) != key2vertices_pub.end();
}

std::unordered_map<int, std::unordered_map<int, std::pair<int, VD>>> PKD(Graph &g, std::vector<VD> portals) {
  std::unordered_map<int, std::unordered_map<int, std::pair<int, VD>>> ans;
  for (auto p: portals) {
    std::unordered_map<int, std::pair<int, VD>> kd_map;

    int hop = 0;
    std::queue<VD> q, next_hop;
    q.push(p);
    while (hop <= 3) {
      while (!q.empty()) {
        VD head = q.front();
        q.pop();

        for (int k: g[head].klist) {
          if (kd_map.find(k) == kd_map.end()) {
            kd_map.insert(std::make_pair(k, std::make_pair(hop, head)));
            continue;
          }
          if (kd_map[k].first > hop) {
            kd_map[k].first = hop;
            continue;
          }
        }

        for (std::pair<EIter, EIter> e = out_edges(head, g); e.first != e.second; e.first++) {
          VD t = target(*e.first, g);
          next_hop.push(t);
        }
      }
      q = next_hop;
      hop++;
      if (q.empty()) {
        break;
      }
    }
    ans.insert(std::make_pair(p, kd_map));
  }
  return ans;
}

std::unordered_map<int, int> loadPortal(std::string path_portal) {
  ifstream portalfile(path_portal + ".p");
  std::unordered_map<int, int> portals;
  int n;
  portalfile >> n;

  for (int i = 0; i < n; i++) {
    int pub, pri;
    portalfile >> pub >> pri;
    portals[pub - 1] = pri - 1;
  }
  return portals;
}

std::unordered_map<VD, std::unordered_map<VD, int>> dist;
std::unordered_map<VD, std::unordered_set<VD>> nindex;

std::unordered_map<VD, std::unordered_set<VD>>
build_r_clique_neighbor_index(Graph &g, int R, std::unordered_set<VD> &tobuild) {
  int cnt = 0;
  for (auto &s: tobuild) {
    if (cnt % 100 == 0)
      LOG(INFO) << cnt << "\tOut of\t" << tobuild.size();
    std::unordered_map<VD, int> d;
    std::unordered_set<VD> neighbor;
    dist[s] = d;

    int hop = 0;

    std::queue<VD> q;
    q.push(s);
    while (hop <= R) {
      std::queue<VD> next_hop;
      while (!q.empty()) {
        VD head = q.front();
        q.pop();
        if (neighbor.find(head) != neighbor.end()) {
          continue;
        }
        neighbor.insert(head);
        dist[s][head] = hop;

        for (std::pair<EIter, EIter> e = out_edges(head, g); e.first != e.second; e.first++) {
          VD t = target(*e.first, g);
          if (neighbor.find(t) != neighbor.end()) {
            continue;
          }
          next_hop.push(t);
        }
      }
      q = next_hop;
      hop++;
      if (q.empty()) {
        break;
      }
    }
    nindex[s] = neighbor;
  }
  return nindex;
}

std::unordered_map<VD, std::unordered_set<VD>> build_r_clique_neighbor_index(Graph &g, int R) {

  for (std::pair<VIter, VIter> p = vertices(g); p.first != p.second; ++p.first) {
    VD s = *p.first;
    std::unordered_map<VD, int> d;
    std::unordered_set<VD> neighbor;
    dist[s] = d;

    int hop = 0;

    std::queue<VD> q;
    q.push(s);
    while (hop < R) {
      std::queue<VD> next_hop;
      while (!q.empty()) {
        VD head = q.front();
        q.pop();
        if (neighbor.find(head) != neighbor.end()) {
          continue;
        }
        neighbor.insert(head);
        dist[s][head] = hop;

        for (std::pair<EIter, EIter> e = out_edges(head, g); e.first != e.second; e.first++) {
          VD t = target(*e.first, g);
          if (neighbor.find(t) != neighbor.end()) {
            continue;
          }
          next_hop.push(t);
        }
      }
      q = next_hop;
      hop++;
      if (q.empty()) {
        break;
      }
    }
    nindex[s] = neighbor;
  }
  return nindex;
}

struct KV {
    VD current;
    VD origin;
};

struct KNKPar {
    int qx;
    int qk;
    int topk;
    int nsize;
    vector<pair<int, int>> ans;
};

vector<KNK> PEvalKNK(KNKPar pars, int qx, int qk) {
  std::unordered_map<int, std::vector<VD>> origins;
  vector<KNK> ans;

  for(auto a: pars.ans){
    KNK aa;
    aa.root = pars.qx;
    aa.target = a.first;
    aa.dist = a.second;
    aa.keyword = pars.qk;
    ans.push_back(aa);
  }
  return ans;
}

void RefineKNK(vector<KNK> &ans, std::unordered_map<int, std::unordered_map<int, int>> dist_pri) {
  for (auto &a: ans) {
    for (auto p1: dist_pri) {
      if (dist[a.root].find(nodes_pri[p1.first]) == dist[a.root].end())
        continue;
      for (auto p2: p1.second) {
        if (dist[nodes_pri[p2.first]].find(a.target) == dist[p2.first].end())
          continue;
        int rem = dist[a.root][nodes_pri[p1.first]] + dist_pri[p1.first][p2.first] +
                  dist[nodes_pri[p2.first]][a.target];
        if (a.dist > rem)
          a.dist = rem;
      }
    }
  }
}

bool operator<(const KNK &p1, const KNK &p2) {
  return p1.dist < p2.dist;
}

void AnsCompl(vector<KNK> &ans, Graph &g_pub, std::unordered_map<int, int> portals, int qx, int qk, int k) {
  if (ans.size() >= k)
    return;
  if (kpads.kpads.find(qk) == kpads.kpads.end())
    return;
  auto qkpads = kpads.kpads[qk];

  priority_queue<KNK> heap;

  for (auto a: ans) {
    heap.push(a);
  }
  for (auto p: portals) {
    if (dist[qx].find(p.first) == dist[qx].end())
      continue;
    int qp_d = dist[qx][p.first];
    auto p_pub = p.second;
    auto p_pads = g_pub[p_pub].pads;

    for (auto wd: p_pads) {
      int w = wd.first;
      int d1 = wd.second;
      if (qkpads.find(w) == qkpads.end())
        continue;
      int d2 = qkpads[w];
      int total_d = qp_d + d1 + d2;
      KNK a;
      a.root = qx;
      a.target = w;
      a.dist = total_d;
      heap.push(a);
    }
  }
  vector<KNK> n_ans;
  for (int i = 0; i < k; i++) {
    if (heap.empty())
      break;
    n_ans.push_back(heap.top());
    heap.pop();
  }
  ans = n_ans;
}

Graph loadGraph(std::string basepath, std::unordered_map<int, VD> &nodes,
                std::unordered_map<int, std::unordered_set<VD>> &key2vertices) {
  Graph g;
  /**
   * Load Vertices
   */
//  ifstream vfile("/Users/samjjx/dev/git/ppkws/cpp/data/sample.v");
  ifstream vfile(basepath + ".v");
  int nv;
  vfile >> nv;


  for (int i = 0; i < nv; i++) {
    int vid, nk;
    vfile >> vid;
    vfile >> nk;
    std::unordered_set<int> klist;

    nodes[i] = add_vertex(g);

    for (int j = 0; j < nk; j++) {
      int k;
      vfile >> k;
      klist.insert(k);
      if (key2vertices.find(k) == key2vertices.end()) {
        std::unordered_set<VD> tmp;
        key2vertices.insert(make_pair(k, tmp));
      }
      key2vertices[k].insert(nodes[i]);
    }
    g[nodes[i]].klist = klist;
  }
  LOG(INFO) << "Finish loading vertices";
  /**
   * Load Edges
   */
  ifstream efile(basepath + ".e");
  int m;
  efile >> m;
  int a, b, weight, i = 0;

  EdgeWeight wedge;
  wedge.weight = 1;

  while (efile >> a && efile >> b && efile >> weight) {
    add_edge(nodes[a - 1], nodes[b - 1], wedge, g);
    add_edge(nodes[b - 1], nodes[a - 1], wedge, g);
  }

  LOG(INFO) << num_edges(g) << std::endl;
  LOG(INFO) << "Finish loading edges" << std::endl;

  return g;
}

void print_stat() {
  int cnt = 0;
  for (auto vid: ads_labels) {
    cnt += vid.second.size();
  }
  std::cout << "Labels Size=\t" << cnt << std::endl;
}

void printKNK(KNK a, Graph &g) {
  LOG(INFO) << "Source \t " << a.root << "\tTarget\t" << a.target << "\tDist\t" << a.dist;
}

void loadQuery(std::string basepath, std::unordered_set<int> &query) {
  ifstream qfile(basepath);
  if (!qfile.is_open()) {
    return;
  }
  int nq;
  qfile >> nq;

  for (int i = 0; i < nq; i++) {
    int q;
    qfile >> q;
    LOG(INFO) << "Query Keywords\t" << q;
    query.insert(q);
  }

}



void loadKNKans(std::string basepath, std::vector<KNKPar> &ans) {
  ifstream afile(basepath);
  if(!afile.is_open()){
    LOG(INFO) << "KNK answers file is illegal";
    return;
  }
  for(int i = 0; i < 20; i++){
    KNKPar par;
    afile >> par.qx >> par.qk >> par.topk >> par.nsize;
    par.qk++;
    for(int j = 0; j < par.nsize; j++){
      int target, d;
      afile >> target >> d;
      par.ans.push_back(make_pair(target, d));
    }
    ans.push_back(par);
  }
  LOG(INFO) << "Finish loading query";
}

std::unordered_set<VD> generate_index_set(std::vector<std::unordered_set<int>> queries,
                                          std::unordered_map<int, std::unordered_set<VD>> &key2vertices) {
  std::unordered_set<VD> allVD;
  for (auto q: queries) {
    for (auto k: q) {
      for (auto v: key2vertices[k]) {
        allVD.insert(v);
      }
    }
  }
  return allVD;
}


int main(int argc, char *argv[]) {
  FLAGS_alsologtostderr = 1;
  FLAGS_colorlogtostderr = 1;
  FLAGS_log_dir = "./";
  google::InstallFailureSignalHandler();
  // 解析命令行参数
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  // 初始化日志库
  google::InitGoogleLogging(argv[0]);

  if (argc <= 3) {
    printf("You must provide at least two argument\n");
    exit(0);
  }
  std::string path_pub = argv[1];
  std::string path_pri = argv[2];
  std::string path_portal = argv[3];

  Graph g_pub = loadGraph(path_pub, nodes_pub, key2vertices_pub);
  Graph g_pri = loadGraph(path_pri, nodes_pri, key2vertices_pri);


  std::unordered_map<int, std::unordered_map<int, int>> dist_pub, dist_pri, changes;
  std::unordered_map<int, int> portals = loadPortal(path_portal);

  std::vector<VD> portal_pri_id;
  std::vector<VD> portal_pub_id;
  for (auto p: portals) {
    portal_pri_id.push_back(nodes_pri[p.first]);
    portal_pub_id.push_back(nodes_pub[p.second]);
  }

  std::unordered_map<int, std::unordered_map<int, std::pair<int, VD>>> pkd = PKD(g_pri, portal_pri_id);

  nindex = build_r_clique_neighbor_index(g_pri, 3);

  PDU(g_pub, g_pri, portals, dist_pub, dist_pri, changes);

  loadADS(argv[4], g_pub, nodes_pub);

  FLAGS_log_dir = argv[6];

  std::vector<KNKPar> answers;
  loadKNKans(argv[5], answers);
  /*
   * Partial Evaluation
   */
  for (int i = 0; i < answers.size(); i++) {
    KNKPar sigmod15 = answers[i];
    LOG(INFO) << "Pattern\t#" << (i + 1);
    LOG(INFO) << "Query INFO\tqx\tqk\ttopk\t" << sigmod15.qx << "\t" << sigmod15.qk << "\t" << sigmod15.topk;
    std::clock_t start;
    double duration = 0;
    double total_time = 0;


    int qx = sigmod15.qx;
    int qk = sigmod15.qk;
    int knk_k = sigmod15.topk;
    start = std::clock();
    auto ans = PEvalKNK(sigmod15, qx, qk);
    duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
    LOG(INFO) << "Size of ans after Partial evaluation " << ans.size();
    LOG(INFO) << "Partial Evaluation time \t" << duration;
    total_time += duration;

    LOG(INFO) << "**************************";
    for (auto a: ans) {
      printKNK(a, g_pri);
    }
    /*
     * Answer Refinement
     */
    start = std::clock();
    RefineKNK(ans, changes);
    LOG(INFO) << "Size of ans after refinement " << ans.size();
    duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
    LOG(INFO) << "Refine time \t" << duration;
    total_time += duration;
//    LOG(INFO) << "Total Time\t" << total_time;
    /*
     * Answer Completion
     */
    start = std::clock();
    AnsCompl(ans, g_pub, portals, qx, qk, knk_k);
    LOG(INFO) << "Size of ans after completion " << ans.size();
    duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
    LOG(INFO) << "Completion time \t" << duration;
    total_time += duration;
    LOG(INFO) << "Total Time\t" << total_time;
    LOG(INFO) << "**************************";
    for (auto a: ans) {
      printKNK(a, g_pri);
    }
  }
  /*
  DLOG(INFO) << "Portal distance in public graph";
  for (auto p1: dist_pub) {
    for (auto p2: dist_pub) {
      DLOG(INFO) << p1.first << "\t" << p2.first << "\t" << dist_pub[p1.first][p2.first];
    }
  }

  DLOG(INFO) << "Portal distance in private graph";
  for (auto p1: dist_pri) {
    for (auto p2: dist_pri) {
      DLOG(INFO) << p1.first << "\t" << p2.first << "\t" << dist_pri[p1.first][p2.first];
    }
  }

  LOG(INFO) << nodes_pub.size() << "\t" << nodes_pri.size();
   */


  /*
  for (auto n : nodes_pub) {
    DLOG(INFO) << "Klist";
    for (auto tmp : g_pub[n.second].klist) {
      DLOG(INFO) << tmp;
    }
    DLOG(INFO) << "PADS";
    for (auto tmp : g_pub[n.second].pads) {
      DLOG(INFO) << tmp.first << "\t" << tmp.second;
    }
  }
  for (int i = 1; i <= 7; i++) {
    DLOG(INFO) << "Keyword 2 vertices";
    for (auto vd :key2vertices_pub[i]) {
      DLOG(INFO) << vd;
    }
  }


  for(auto kwd: kpads.kpads){
    DLOG(INFO) << "Keywords\t" << kwd.second.size();
    DLOG(INFO) << kwd.first;
    for(auto wd: kwd.second){
      DLOG(INFO) << wd.first + 1 << "\t" << wd.second;
    }
  }
   */

  google::ShutdownGoogleLogging();
  google::ShutDownCommandLineFlags();
  return 0;
}
