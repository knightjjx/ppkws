#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <utility>
#include <algorithm>
#include <ctime>
#include <random>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/page_rank.hpp>

#include <glog/logging.h>
#include "io/Reader.hpp"


using namespace boost;

struct pads {
    // distance map: (w,d)
    std::unordered_map<int, int> pads;

    int vid;
};

struct EdgeWeight {
    int weight;
};
typedef adjacency_list<vecS, vecS, directedS, pads, property<edge_weight_t, EdgeWeight>> Graph;
typedef property_map<Graph, vertex_index_t>::type IndexMap;
typedef iterator_property_map<std::vector<double>::iterator, IndexMap> RankMap;
typedef graph_traits<Graph>::vertex_iterator VIter;
typedef graph_traits<Graph>::out_edge_iterator EIter;
typedef graph_traits<Graph>::vertex_descriptor vertex_descriptor;
typedef graph_traits<Graph>::edge_descriptor edge_descriptor;
typedef std::pair<int, int> Edge;
typedef graph_traits<Graph>::vertex_descriptor VD;

using namespace std;

int k = 1;

std::unordered_map<int, VD> nodes;
std::unordered_map<int, std::unordered_map<int, int>> ads_labels;

void printLabels(std::unordered_map<int, std::unordered_map<int, int>> ads) {
  for (auto l: ads) {
    std::cout << l.first << ":\t";
    for (auto label: l.second) {
      std::cout << "(" << label.first << "," << label.second << ")" << "\t";
    }
    std::cout << std::endl;
  }
}


std::vector<int> dijkstra(Graph &g, int source) {
  priority_queue<pair<int, int>> heap;
  std::unordered_set<int> visited;
  std::vector<int> dist(num_vertices(g));

  for (int i = 0; i < nodes.size(); i++) {
    dist[i] = std::numeric_limits<int>::max();
  }

  dist[source] = 0;
  heap.push(make_pair(0, source));

  while (!heap.empty()) {
    int u = heap.top().second;
    int dis = -heap.top().first;
    heap.pop();

    if (visited.find(u) != visited.end()) {
      continue;
    }

    visited.insert(u);

    EIter i, end;

    for (tie(i, end) = out_edges(nodes[u], g); i != end; i++) {
      auto v = target(*i, g);
      auto len = get(edge_weight_t(), g, *i).weight;

      if (dist[v] > dist[u] + len) {
        dist[v] = dist[u] + len;
        heap.push(make_pair(-dist[v], v));
      }
    }
  }
  return dist;
}

int estimate(int src, int dst) {
  auto s_ads = ads_labels[src];
  auto t_ads = ads_labels[dst];
  int min = std::numeric_limits<int>::max();
  for (auto e: s_ads) {
    if (t_ads.find(e.first) != t_ads.end()) {
      int tmp = t_ads[e.first] + e.second;
      min = tmp < min ? tmp : min;
    }
  }
  return min;
}

void test(Graph g, int times) {
  int n = num_vertices(g);
  long count = 0;
  double ratio = 0;
  for (int i = 0; i < times; i++) {
    int source = rand() % n;

//    std::cout << source << std::endl;
    std::vector<int> dist = dijkstra(g, source);
    int cnt = 0;
    for (auto real: dist) {
      int est = estimate(source + 1, cnt += 1);
      if (real == std::numeric_limits<int>::max() || est == std::numeric_limits<int>::max())
        continue;
      if (real == 0)
        continue;
      int tmp_ratio = (double) est / real;
      ratio += tmp_ratio;
      count++;
      if (count % 100000 == 0)
        LOG(INFO) << "Approximate ratio\t" << ratio / count;
    }
  }
}

void loadADS(std::string adspath) {
  ifstream adsfile(adspath);
  std::string line;
  while (std::getline(adsfile, line)) {
    std::istringstream s(line);
    int vid, w, d;
    s >> vid;
    std::unordered_map<int, int> tmp;

    while ((s >> w >> d)) {
      tmp[w] = d;
    }
    ads_labels[vid] = tmp;
  }
}

Graph loadGraph(std::string basepath) {
  Graph g;
  /**
   * Load Vertices
   */
//  ifstream vfile("/Users/samjjx/dev/git/ppkws/cpp/data/sample.v");
  ifstream vfile(basepath + ".v");
  int nv;
  vfile >> nv;


  for (int i = 0; i < nv; i++) {
    int vid, nk;
    vfile >> vid;
    vfile >> nk;
    std::unordered_set<int> klist;

    nodes[i] = add_vertex(g);

    for (int j = 0; j < nk; j++) {
      int k;
      vfile >> k;
      klist.insert(k);
    }
//    std::cout << klist.size() << std::endl;
  }
  std::cout << "Finish loading vertices" << std::endl;
  /**
   * Load Edges
   */
  ifstream efile(basepath + ".e");
  int m;
  efile >> m;
  int a, b, weight, i = 0;

  EdgeWeight wedge;
  wedge.weight = 1;

  while (efile >> a && efile >> b && efile >> weight) {
    add_edge(nodes[a - 1], nodes[b - 1], wedge, g);
    add_edge(nodes[b - 1], nodes[a - 1], wedge, g);
  }

  LOG(INFO) << "# of edges\t" << num_edges(g);
  LOG(INFO) << "# of vertices\t" << num_vertices(g);
  LOG(INFO) << "Finish loading edges";

//  assert(num_vertices(g) == 13);
//  assert(num_edges(g) == 28);
  return g;
}

void print_stat() {
  int cnt = 0;
  for (auto vid: ads_labels) {
    cnt += vid.second.size();
  }
  std::cout << "Labels Size=\t" << cnt << std::endl;
}


int main(int argc, char *argv[]) {
  FLAGS_alsologtostderr = 1;
  FLAGS_colorlogtostderr = 1;
  // 解析命令行参数
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  // 初始化日志库
  google::InitGoogleLogging(argv[0]);

  if (argc <= 2) {
    printf("You must provide at least two argument\n");
    exit(0);
  }
  std::string pathbase = argv[1];
  Graph g = loadGraph(pathbase);

//  Graph g = loadGraph("/Users/samjjx/dev/git/ppkws/cpp/data/sample");
//  Graph g = loadGraph("/Users/samjjx/dev/git/ppkws-datasets/yago_ppkws");
//  Graph g = loadGraph("/home/jxjian/ppkws-datasets/yago_ppkws");

//  loadADS("/Users/samjjx/dev/git/ppkws/cpp/data/sample.ads");
//  loadADS("/home/jxjian/ppkws-datasets/yago.ads");
  loadADS(argv[2]);
  std::clock_t start;
  double duration;

  start = std::clock();

  duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;

  LOG(INFO) << "Running time\t" << duration;

  test(g, 10000);
//  printLabels(ads_labels);
//  print_stat();

  google::ShutdownGoogleLogging();
  google::ShutDownCommandLineFlags();

  return 0;
}
