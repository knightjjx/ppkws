//
// Created by samjjx on 2019-07-14.
//
#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <utility>
#include <algorithm>
#include <ctime>
#include <random>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/page_rank.hpp>

#include <glog/logging.h>
#include "io/Reader.hpp"
#import <boost/serialization/unordered_map.hpp>
#import <boost/serialization/unordered_set.hpp>
#import <boost/serialization/vector.hpp>


#include <boost/serialization/serialization.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

using namespace std;
struct my_struct { int value=-1; };
struct T{
    std::unordered_map<int, int> a;
};

void Descartes(const std::vector < std::string >& s)
{
  int count = 1;
  for (auto a : s)
  {
    count *= a.size();
  }
  for (int i = 0; i < count; i++)
  {
    int quo = i;
    int rem = 0;
    for (auto k=s.rbegin();k!=s.rend();k++)
    {
      rem = quo % (k->size());
      quo = quo / (k->size());
      printf("%c", (*k)[rem]);
    }
    printf("\n");
  }
}
void change(std::vector<T> &ts){
  for(auto & t: ts){
    t.a[5] = 66;
  }
}

void set1(int** n, int row, int col){
  for(int i=0;i<row;i++)
    for(int j=0;j<col;j++){
      n[i][j] = 2;
    }
}
int main(int argc, char* argv[]) {
  FLAGS_alsologtostderr = 1;
  FLAGS_colorlogtostderr = 1;
  // 解析命令行参数
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  // 初始化日志库
  google::InitGoogleLogging(argv[0]);

  std::stringstream ss;
//  Descartes(s);
  T t;
  unordered_map<int, int> tmp1;
  tmp1.insert(std::make_pair(5, 15));
  tmp1.insert(std::make_pair(6, 16));


  unordered_map<int, int> tmp2;
  tmp2.insert(std::make_pair(1, 11));
  tmp2.insert(std::make_pair(5, 12));


  std::vector<std::unordered_map<int, int> > tests, testsin;
  tests.push_back(tmp1);
  tests.push_back(tmp2);

  unordered_map<int, unordered_map<int, int>> a;


  std::clock_t start, start1;
  double duration, duration1;
  start = std::clock();


  int rows = 2;
  int cols = 2;
  int **n2Arr = new int *[rows];
  for (int i = 0; i < rows; i++) {
    n2Arr[i] = new int[cols];
    for (int j = 0; j < cols; j++)
      n2Arr[i][j] = 1;
  }

  set1(n2Arr, rows, cols);

  for (int i = 0; i < rows; i++)
    for (int j = 0; j < cols; j++) {
      LOG(INFO) << n2Arr[i][j];
    }


  duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
  LOG(INFO) << "Evaluation time \t" << duration;

  for(int j=0;j<rows;j++)
    delete[] n2Arr[j];
  delete[] n2Arr;


  int cnt = 0;
  unordered_map<int, int> aa;
  for(int i =0; i< 20000; i++){
    aa[i] = 0;
  }
  for(int i =0;i < 20000;i++){
    start1 = std::clock();
    unordered_map<int, int> tmp(aa.begin(), aa.end());
    duration1 = (std::clock() - start1) / (double) CLOCKS_PER_SEC;
//    LOG(INFO) << "Evaluation time \t" << duration1;
  }


  return 0;
}