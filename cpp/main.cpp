#include <iostream>
#include <fstream>
#include <queue>
#include <utility>
#include <algorithm>
#include <ctime>
#include <random>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/page_rank.hpp>

#include <glog/logging.h>
#include "io/Reader.hpp"


using namespace boost;

struct pads {
    // distance map: (w,d)
    std::unordered_map<int, int> pads;

    int vid;
};

struct EdgeWeight {
    int weight;
};
typedef adjacency_list<vecS, vecS, directedS, pads, property<edge_weight_t, EdgeWeight>> Graph;
typedef property_map<Graph, vertex_index_t>::type IndexMap;
typedef iterator_property_map<std::vector<double>::iterator, IndexMap> RankMap;
typedef graph_traits<Graph>::vertex_iterator VIter;
typedef graph_traits<Graph>::out_edge_iterator EIter;
typedef graph_traits<Graph>::vertex_descriptor vertex_descriptor;
typedef graph_traits<Graph>::edge_descriptor edge_descriptor;
typedef std::pair<int, int> Edge;
typedef graph_traits<Graph>::vertex_descriptor VD;

using namespace std;

int k = 1;

std::unordered_map<int, VD> nodes;
std::unordered_map<int, std::unordered_map<int, int>> ads_labels;

std::vector<double> prvalue(Graph g) {
  std::vector<double> ranks(num_vertices(g));
  page_rank(g, make_iterator_property_map(ranks.begin(), get(vertex_index, g)),
            graph::n_iterations(1000), 1, num_vertices(g));

  /*
  for (auto v: ranks) {
    std::cout << v << std::endl;
  }
  std::cout << "\n" << std::endl;
   */
  return ranks;
}

std::vector<double> adsrandomevalue(Graph g) {
  std::vector<double> ranks(num_vertices(g));
  for (int i = 0; i < ranks.size(); ++i) {
   ranks[i] = ((double) rand() / (RAND_MAX));
  }
  return ranks;
}

void printLabels(std::unordered_map<int, std::unordered_map<int, int>> ads) {
  for (auto l: ads) {
    std::cout << l.first + 1 << ":\t";
    for (auto label: l.second) {
      std::cout << "(" << label.first + 1 << "," << label.second << ")" << "\t";
    }
    std::cout << std::endl;
  }
}

void writeLabel(std::string labelpath){
  ofstream myfile;
  myfile.open(labelpath);
  for (auto l: ads_labels) {
    myfile << l.first + 1  << "\t";
    int cnt = 0;
    for (auto label: l.second) {
      cnt ++;
      if (cnt == l.second.size())
        myfile << label.first +1 << "\t" << label.second << "\n";
      else
        myfile << label.first +1 << "\t" << label.second << "\t";
    }
  }
  myfile.flush();
  myfile.close();
}

bool IScenter(int dist, int v) {
  auto ads = ads_labels[v];
  int cnt = 0;
  for (auto e: ads) {
    if (e.second <= dist)
      cnt++;
    if (cnt >= k)
      return false;
  }
  return true;
}

void prune_dijkstra(Graph &g, int source) {
  priority_queue<pair<int, int>> heap;
  std::unordered_set<int> visited;
  std::vector<int> dist(num_vertices(g));

  for (int i = 0; i < nodes.size(); i++) {
    dist[i] = std::numeric_limits<int>::max();
  }

  dist[source] = 0;
  heap.push(make_pair(0, source));

  while (!heap.empty()) {
    int u = heap.top().second;
    int dis = -heap.top().first;
    heap.pop();

    if (visited.find(u) != visited.end()) {
      continue;
    }

    if (IScenter(dis, u)) {
      ads_labels[u][source] = dis;
    } else {
      visited.insert(u);
      continue;
    }

    visited.insert(u);

    EIter i, end;

    for (tie(i, end) = out_edges(nodes[u], g); i != end; i++) {
      auto v = target(*i, g);
      auto len = get(edge_weight_t(), g, *i).weight;

      if (dist[v] > dist[u] + len) {
        dist[v] = dist[u] + len;
        heap.push(make_pair(-dist[v], v));
      }
    }
  }

  return;
}

void prads(Graph &g, std::vector<double> ranks) {
  std::map<int, double> rmap;

  for (int i = 0; i < ranks.size(); ++i) {
    rmap[i] = ranks[i];
    std::unordered_map<int, int> tmp;
    ads_labels[i] = tmp;
  }

  // Declaring the type of Predicate that accepts 2 pairs and return a bool
  typedef std::function<bool(std::pair<int, double>, std::pair<int, double>)> Comparator;

  Comparator compFunctor =
          [](std::pair<int, double> elem1, std::pair<int, double> elem2) {
              return elem1.second >= elem2.second;
          };
  std::set<std::pair<int, double>, Comparator> decending_order_rank(
          rmap.begin(), rmap.end(), compFunctor);
  int cnt = 0;
  for (auto element : decending_order_rank) {
//    std::cout << element.first << " :: " << element.second << std::endl;
    if (cnt++ % 10000 == 0) {
      LOG(INFO) << "Finish\t" << cnt;
    }
    prune_dijkstra(g, element.first);
  }
  return;
}

Graph loadGraph(std::string basepath) {
  Graph g;
  /**
   * Load Vertices
   */
//  ifstream vfile("/Users/samjjx/dev/git/ppkws/cpp/data/sample.v");
  ifstream vfile(basepath + ".v");
  int nv;
  vfile >> nv;


  for (int i = 0; i < nv; i++) {
    int vid, nk;
    vfile >> vid;
    vfile >> nk;
    std::unordered_set<int> klist;

    nodes[i] = add_vertex(g);

    for (int j = 0; j < nk; j++) {
      int k;
      vfile >> k;
      klist.insert(k);
    }
//    std::cout << klist.size() << std::endl;
  }
  LOG(INFO) << "Finish loading vertices" << std::endl;
  /**
   * Load Edges
   */
  ifstream efile(basepath + ".e");
  int m;
  efile >> m;
  int a, b, weight, i = 0;

  EdgeWeight wedge;
  wedge.weight = 1;

  while (efile >> a && efile >> b && efile >> weight) {
    add_edge(nodes[a-1], nodes[b-1], wedge, g);
    add_edge(nodes[b-1], nodes[a-1], wedge, g);
  }

  LOG(INFO) << "# of edges\t" << num_edges(g);
  LOG(INFO) << "# of vertices\t" << num_vertices(g);
  LOG(INFO) << "Finish loading edges";
  assert(num_vertices(g) == 13);
  assert(num_edges(g) == 28);
  return g;
}

void print_stat(){
  int cnt = 0;
  for(auto vid: ads_labels){
    cnt += vid.second.size();
  }
  LOG(INFO) << "Labels Size\t" << cnt << std::endl;
}

int main(int argc, char *argv[]) {

  FLAGS_alsologtostderr = 1;
  FLAGS_colorlogtostderr = 1;
  // 解析命令行参数
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  // 初始化日志库
  google::InitGoogleLogging(argv[0]);


//  Graph g = loadGraph("/Users/samjjx/dev/git/ppkws/cpp/data/sample");
//  Graph g = loadGraph("/Users/samjjx/dev/git/ppkws-datasets/yago_ppkws");
//  Graph g = loadGraph("/home/jxjian/ppkws-datasets/yago_ppkws");


  if(argc <= 2) {
    printf("You must provide at least two argument\n");
    exit(0);
  }
  VLOG(1) << "Testing";
  std::string pathbase = argv[1];
  Graph g = loadGraph(pathbase);
  k = atoi(argv[2]);

  std::vector<double> pagerankvalue;
  pagerankvalue = prvalue(g);
  LOG(INFO) << "Page Rank computed";
  std::clock_t start;
  double duration;

  start = std::clock();

  prads(g, pagerankvalue);

  duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;

  LOG(INFO) << "Index Construction time\t" << duration;
//  printLabels(ads_labels);
  writeLabel(pathbase + "-k" + std::to_string(k) + ".pads");

  print_stat();

  google::ShutdownGoogleLogging();
  google::ShutDownCommandLineFlags();
  return 0;
}
