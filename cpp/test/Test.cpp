//
// Created by samjjx on 2019/4/30.
//

#include "../io/Reader.hpp"
#include <glog/logging.h>

int main(int argc, char* argv[])
{
  FLAGS_alsologtostderr = 1;
  FLAGS_colorlogtostderr = 1;
  // 解析命令行参数
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  // 初始化日志库
  google::InitGoogleLogging(argv[0]);

  LOG(INFO) << "Hello, World!";


  google::ShutdownGoogleLogging();
  google::ShutDownCommandLineFlags();
}