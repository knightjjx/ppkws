#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <utility>
#include <algorithm>
#include <ctime>
#include <random>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/page_rank.hpp>

#include <glog/logging.h>
#include "io/Reader.hpp"


using namespace boost;



struct VertexP {
    // distance map: (w,d)
    std::unordered_map<int, int> pads;
    std::unordered_set<int> klist;
    int vid;
};

struct EdgeWeight {
    int weight;
};

typedef adjacency_list<vecS, vecS, directedS, VertexP, property<edge_weight_t, EdgeWeight>> Graph;
typedef property_map<Graph, vertex_index_t>::type IndexMap;
typedef iterator_property_map<std::vector<double>::iterator, IndexMap> RankMap;
typedef graph_traits<Graph>::vertex_iterator VIter;
typedef graph_traits<Graph>::out_edge_iterator EIter;
typedef graph_traits<Graph>::edge_descriptor edge_descriptor;
typedef std::pair<int, int> Edge;
typedef graph_traits<Graph>::vertex_descriptor VD;

struct KPADS {
    std::unordered_map<int, std::unordered_map<int,int>> kpads;
};

struct rclique {
    VD source;
    std::unordered_set<int> query;
    // Improve the refine performance
    std::unordered_map<VD, int> d; // vertex -> distance

    // For completion
    std::unordered_map<int, VD> match; // keyword -> vertex
    std::unordered_map<int, int> dkeyword; // keyword -> distance
    std::unordered_map<int, bool> isportal; // keyword -> portal
    int total_weight = 0;
};

using namespace std;

int k = 1;
KPADS kpads;


std::unordered_map<int, VD> nodes_pub;
std::unordered_map<int, VD> nodes_pri;
std::unordered_map<int, std::unordered_map<int, int>> ads_labels;
std::unordered_map<int, std::unordered_set<VD>> key2vertices_pub;
std::unordered_map<int, std::unordered_set<VD>> key2vertices_pri;
//
//int** pub_dist;
//int** pri_dist;

std::vector<int> dijkstra(Graph &g, std::unordered_map<int, VD> &nodes, int source) {
  priority_queue<pair<int, int>> heap;
  std::unordered_set<int> visited;
  std::vector<int> dist(num_vertices(g));

  for (int i = 0; i < nodes.size(); i++) {
    dist[i] = std::numeric_limits<int>::max();
  }

  dist[source] = 0;
  heap.push(make_pair(0, source));

  while (!heap.empty()) {
    int u = heap.top().second;
    int dis = -heap.top().first;
    heap.pop();

    if (visited.find(u) != visited.end()) {
      continue;
    }

    visited.insert(u);

    EIter i, end;

    for (tie(i, end) = out_edges(nodes[u], g); i != end; i++) {
      auto v = target(*i, g);
      auto len = get(edge_weight_t(), g, *i).weight;

      if (dist[v] > dist[u] + len) {
        dist[v] = dist[u] + len;
        heap.push(make_pair(-dist[v], v));
      }
    }
  }
  return dist;
}

/*
 * The portal distance refinement
 */
void PDU(Graph g_pub, Graph g_pri, std::unordered_map<int, int> portals,
         std::unordered_map<int, std::unordered_map<int, int>> &dist_pub,
         std::unordered_map<int, std::unordered_map<int, int>> &dist_pri) {
  for (auto p: portals) {
    std::unordered_map<int, int> m_pri, m_pub;
    dist_pri[p.first] = m_pri;
    dist_pub[p.second] = m_pub;
  }
  for (auto p1: dist_pri) {
    for (auto p2: dist_pri) {
      if (p1.first == p2.first) {
        dist_pri[p1.first][p2.first] = 0;
        continue;
      }
      dist_pri[p1.first][p2.first] = std::numeric_limits<int>::max();
    }
  }
  for (auto p1: dist_pub) {
    for (auto p2: dist_pub) {
      if (p1.first == p2.first) {
        dist_pub[p1.first][p2.first] = 0;
        continue;
      }
      dist_pub[p1.first][p2.first] = std::numeric_limits<int>::max();
    }
  }

  for (auto p1: dist_pub) {
    std::vector<int> d_pub = dijkstra(g_pub, nodes_pub, p1.first);
    for (auto p2: dist_pub) {
      dist_pub[p1.first][p2.first] = d_pub[p2.first];
    }
  }

  for (auto p1: dist_pri) {
    std::vector<int> d_pri = dijkstra(g_pri, nodes_pri, p1.first);
    for (auto p2: dist_pri) {
      dist_pri[p1.first][p2.first] = d_pri[p2.first];
    }
  }

  std::clock_t start;
  double duration;

  start = std::clock();

  priority_queue<pair<int, pair<int, int>>> heap;
  for (auto p1: dist_pri) {
    for (auto p2: dist_pri) {
      if (dist_pri[p1.first][p2.first] > dist_pub[portals[p1.first]][portals[p2.first]]) {
        dist_pri[p1.first][p2.first] = dist_pub[portals[p1.first]][portals[p2.first]];
        heap.push(make_pair(-dist_pri[p1.first][p2.first], make_pair(p1.first, p2.first)));
      }
    }
  }

  while (!heap.empty()) {
    int dist = -heap.top().first;
    int p1 = heap.top().second.first;
    int p2 = heap.top().second.second;

    heap.pop();

    for (auto pi: portals) {
      if (dist_pri[pi.first][p1] != std::numeric_limits<int>::max() &&
          dist_pri[pi.first][p1] + dist < dist_pri[pi.first][p2]) {
        dist_pri[pi.first][p2] = dist_pri[pi.first][p1] + dist;
        heap.push(make_pair(-dist_pri[pi.first][p2], make_pair(pi.first, p2)));
      }
      if (dist_pri[pi.first][p2] != std::numeric_limits<double>::max() &&
          dist_pri[pi.first][p2] + dist < dist_pri[pi.first][p1]) {
        dist_pri[pi.first][p1] = dist_pri[pi.first][p2] + dist;
        heap.push(make_pair(-dist_pri[pi.first][p1], make_pair(pi.first, p1)));
      }
    }
  }
  duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
  LOG(INFO) << "Construction time \t" << duration;
}

void loadADS(std::string adspath, Graph &g, std::unordered_map<int, VD> &nodes) {
  ifstream adsfile(adspath);
  std::string line;
  while (std::getline(adsfile, line)) {
    std::istringstream s(line);
    int vid, w, d;
    s >> vid;
    std::unordered_map<int, int> tmp;

    while ((s >> w >> d)) {
      tmp[w - 1] = d;
    }
    g[nodes[vid - 1]].pads = tmp;
  }

  /*
   * Generate KPADS by PADS
   */
  for (auto kv: key2vertices_pub) {
    auto keyword = kv.first;
    if (kpads.kpads.find(keyword) == kpads.kpads.end()) {
      std::unordered_map<int,int> ads;
      kpads.kpads.insert(std::make_pair(keyword, ads));
    }
    auto VDs = kv.second;

    std::unordered_map<int, int> toMerge;
    for (auto v: VDs) {
      for (auto wd: g[v].pads) {
        auto w = wd.first;
        auto d = wd.second;
        if(toMerge.find(w)==toMerge.end())
          toMerge.insert(std::make_pair(w,d));
        else{
          toMerge[w] = toMerge[w] > d ? d : toMerge[w];
        }
        /* Merge the common centers
        if (kpads.kpads[keyword].find(w) == kpads.kpads[keyword].end()) {
          kpads.kpads[keyword].insert(std::make_pair(w, d));
        } else {
          if (kpads.kpads[keyword][w] > d)
            kpads.kpads[keyword][w] = d;
        }
        */
      }
    }
    DLOG(INFO) << "Size of Merge" << toMerge.size();
    for(auto wd: toMerge) {
      auto w = wd.first;
      auto d = wd.second;
      kpads.kpads[keyword].insert(std::make_pair(w, d));
    }
  }
}

std::unordered_map<int, int> loadPortal(std::string path_portal) {
  ifstream portalfile(path_portal + ".p");
  std::unordered_map<int, int> portals;
  int n;
  portalfile >> n;

  for (int i = 0; i < n; i++) {
    int pub, pri;
    portalfile >> pub >> pri;
    portals[pub - 1] = pri - 1;
  }
  return portals;
}

std::unordered_map<VD, std::unordered_map<VD, int>> dist;
std::unordered_map<VD, std::unordered_set<VD>> nindex;

std::unordered_map<VD, std::unordered_set<VD>> build_r_clique_neighbor_index(Graph &g, int R) {

  for (std::pair<VIter, VIter> p = vertices(g); p.first != p.second; ++p.first) {
    VD s = *p.first;
    std::unordered_map<VD, int> d;
    std::unordered_set<VD> neighbor;
    dist[s] = d;

    int hop = 0;

    std::queue<VD> q;
    q.push(s);
    while (hop < R) {
      std::queue<VD> next_hop;
      while (!q.empty()) {
        VD head = q.front();
        q.pop();
        if (neighbor.find(head) != neighbor.end()) {
          continue;
        }
        neighbor.insert(head);
        dist[s][head] = hop;

        for (std::pair<EIter, EIter> e = out_edges(head, g); e.first != e.second; e.first++) {
          VD t = target(*e.first, g);
          if (neighbor.find(t) != neighbor.end()) {
            continue;
          }
          next_hop.push(t);
        }
      }
      q = next_hop;
      hop++;
      if (q.empty()) {
        break;
      }
    }
    nindex[s] = neighbor;
  }
  return nindex;
}

vector<rclique> PEvalRclique(Graph &g, std::unordered_map<int, std::unordered_set<VD>> &key2vertices, std::vector<VD> P,
                             std::unordered_set<int> query) {
  std::unordered_map<int, std::vector<VD>> SPs;

  /*
   * Init the search Space and Append the portal sets
   */
  for (auto key: query) {
    std::vector<VD> tmp;
    SPs.insert(std::make_pair(key, tmp));
    if (key2vertices.find(key) == key2vertices.end()) {
      SPs[key].insert(SPs[key].end(), P.begin(), P.end());
      continue;
    }
    std::vector<VD> key2vertices_V(key2vertices[key].begin(), key2vertices[key].end());
    SPs[key].insert(SPs[key].end(), key2vertices_V.begin(), key2vertices_V.end());
    SPs[key].insert(SPs[key].end(), P.begin(), P.end());
  }

  int cnt = 1;
  for (auto SP: SPs) {
    cnt *= SP.second.size();
  }
  vector<rclique> ans;

  for (int i = 0; i < cnt; i++) {
    int point = i;
    rclique a;
    for (auto SP: SPs) {
      auto keyword = SP.first;
      int first = point % SP.second.size();
      point = point / SP.second.size();
      a.query.insert(SP.first);
      a.match.insert(make_pair(SP.first, SP.second[first]));
      a.isportal.insert(
              make_pair(keyword, key2vertices[keyword].find(SP.second[first]) == key2vertices[keyword].end()));
    }
    bool flag = false;
    for (auto m: a.isportal) {
      if (m.second)
        continue;
      if (!m.second) {
        flag = true;
        a.source = a.match[m.first];
        break;
      }
    }

    for (auto m: a.match) {
      auto t = m.second;
      if (nindex[a.source].find(t) != nindex[a.source].end()) {
        a.d[t] = dist[a.source][t];
      } else {
        a.d[t] = std::numeric_limits<int>::max();
      }
    }

    if (flag)
      ans.push_back(a);
  }
  return ans;
}

void RefineRclique(vector<rclique> &ans, std::unordered_map<int, std::unordered_map<int, int>> dist_pri,
                   int R = 3) {
  for (auto & a: ans) {
    for (auto p1: dist_pri) {
      if (dist[a.source].find(nodes_pri[p1.first]) == dist[a.source].end())
        continue;
      for (auto p2: p1.second) {
        if (p2.second > R)
          continue;
        for (auto m: a.d) {
          if (dist[nodes_pri[p2.first]].find(m.first) == dist[p2.first].end())
            continue;
          int rem = dist[a.source][nodes_pri[p1.first]] + dist_pri[p1.first][p2.first] +
                    dist[nodes_pri[p2.first]][m.first];
          if(a.d[m.first] > rem)
            a.d[m.first] = rem;
        }
      }
    }
  }
}

void AnsCompl(vector<rclique> &ans, Graph &g_pub, std::unordered_map<int, int> portals, std::unordered_set<int> query, int R = 3) {
  vector<rclique> completedAns;
  std::unordered_map<int, std::unordered_map<int,int>> pktable; // id_pub -> keyword -> distance

  for(auto p: portals){
    auto vd_pads = g_pub[p.second].pads;
    for (auto k: query){
      int tmp = std::numeric_limits<int>::max();
      std::unordered_map<int,int> kpads_k = kpads.kpads[k];

      for(auto wd: vd_pads){
        auto w = wd.first;
        auto d = wd.second;
        if(kpads_k.find(w)==kpads_k.end())
          continue;
        int d_pub = kpads_k[w] + d;
        tmp = tmp > d_pub ? d_pub : tmp;
      }
      pktable[p.second][k] = tmp;
    }
  }

  for (auto a: ans) {
    bool flag = true;
    for (auto kv: a.match) {
      auto k = kv.first;
      auto vd = kv.second;
      a.dkeyword[k] = a.d[a.match[k]];
      if (a.isportal[k]) {
        auto vd_pub = portals[vd];
        if(pktable[vd_pub][k] == std::numeric_limits<int>::max() || pktable[vd_pub][k] + a.dkeyword[k] > R){
          flag = false;
          break;
        }
        a.dkeyword[k] = pktable[vd_pub][k] + a.dkeyword[k];
        if(a.dkeyword[k] < 0){
          LOG(INFO) << "WARNING";
        }
      }
    }
    if(flag)
      completedAns.push_back(a);
  }
  ans = completedAns;
}

Graph loadGraph(std::string basepath, std::unordered_map<int, VD> &nodes,
                std::unordered_map<int, std::unordered_set<VD>> &key2vertices) {
  Graph g;
  /**
   * Load Vertices
   */
//  ifstream vfile("/Users/samjjx/dev/git/ppkws/cpp/data/sample.v");
  ifstream vfile(basepath + ".v");
  int nv;
  vfile >> nv;


  for (int i = 0; i < nv; i++) {
    int vid, nk;
    vfile >> vid;
    vfile >> nk;
    std::unordered_set<int> klist;

    nodes[i] = add_vertex(g);

    for (int j = 0; j < nk; j++) {
      int k;
      vfile >> k;
      klist.insert(k);
      if (key2vertices.find(k) == key2vertices.end()) {
        std::unordered_set<VD> tmp;
        key2vertices.insert(make_pair(k, tmp));
      }
      key2vertices[k].insert(nodes[i]);
    }
    g[nodes[i]].klist = klist;
  }
  LOG(INFO) << "Finish loading vertices";
  /**
   * Load Edges
   */
  ifstream efile(basepath + ".e");
  int m;
  efile >> m;
  int a, b, weight, i = 0;

  EdgeWeight wedge;
  wedge.weight = 1;

  while (efile >> a && efile >> b && efile >> weight) {
    add_edge(nodes[a - 1], nodes[b - 1], wedge, g);
    add_edge(nodes[b - 1], nodes[a - 1], wedge, g);
  }

  LOG(INFO) << num_edges(g) << std::endl;
  LOG(INFO) << "Finish loading edges" << std::endl;

  return g;
}

void print_stat() {
  int cnt = 0;
  for (auto vid: ads_labels) {
    cnt += vid.second.size();
  }
  std::cout << "Labels Size=\t" << cnt << std::endl;
}

void printRclique(rclique a, Graph& g){
  LOG(INFO) << "**************************";
  LOG(INFO) << "Source \t " <<a.source<< "\t" << g[a.source].klist.size();
  for(auto vd: a.dkeyword){
    LOG(INFO) << "Keyword\t" << vd.first << "\t dist\t " << vd.second;
  }
  for(auto kv: a.match){
    LOG(INFO) << "Keyword\t" << kv.first << "\t Vertex\t " << kv.second;
  }
  for(auto vd: a.d){
    LOG(INFO) << "vid\t" << vd.first << "\t dist\t " << vd.second;
  }
  LOG(INFO) << "**************************";
}

void loadQuery(std::string basepath, std::unordered_set<int>& query){
  ifstream qfile(basepath);
  int nq;
  qfile >> nq;

  for(int i = 0;i<nq;i++){
    int q;
    qfile >> q;
    query.insert(q);
  }
  LOG(INFO) << "Finish loading query";

}

int main(int argc, char *argv[]) {
  FLAGS_alsologtostderr = 1;
  FLAGS_colorlogtostderr = 1;
  // 解析命令行参数
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  // 初始化日志库
  google::InitGoogleLogging(argv[0]);

  if (argc <= 3) {
    printf("You must provide at least two argument\n");
    exit(0);
  }
  std::string path_pub = argv[1];
  std::string path_pri = argv[2];
  std::string path_portal = argv[3];

  Graph g_pub = loadGraph(path_pub, nodes_pub, key2vertices_pub);
  Graph g_pri = loadGraph(path_pri, nodes_pri, key2vertices_pri);

  std::unordered_map<int, std::unordered_map<int, int>> dist_pub, dist_pri;
  std::unordered_map<int, int> portals = loadPortal(path_portal);

  std::vector<VD> portal_pri_id;
  std::vector<VD> portal_pub_id;
  for (auto p: portals) {
    portal_pri_id.push_back(nodes_pri[p.first]);
    portal_pub_id.push_back(nodes_pub[p.second]);
  }

  PDU(g_pub, g_pri, portals, dist_pub, dist_pri);

  loadADS(argv[4], g_pub, nodes_pub);

  std::unordered_set<int> query;
  loadQuery(argv[5], query);

//  query.insert(4);


  nindex = build_r_clique_neighbor_index(g_pri, 3);
  /*
   * Partial Evaluation
   */
  std::clock_t start;
  double duration;
  start = std::clock();
  auto ans = PEvalRclique(g_pri, key2vertices_pri, portal_pri_id, query);
  duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
  LOG(INFO) << "Partial Evaluation time \t" << duration;

  for(auto a: ans){
    printRclique(a, g_pri);
  }
  /*
   * Answer Refinement
   */
  start = std::clock();
  RefineRclique(ans, dist_pri, 3);
  LOG(INFO) << "Size of ans after refinement " << ans.size();
  duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
  LOG(INFO) << "Refine time \t" << duration;
  for(auto a: ans){
    printRclique(a, g_pri);
  }

  /*
   * Answer Completion
   */
  start = std::clock();
  AnsCompl(ans, g_pub, portals, query, 3);
  LOG(INFO) << "Size of ans after completion " << ans.size();
  duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
  LOG(INFO) << "Completion time \t" << duration;

  for(auto a: ans){
    printRclique(a, g_pri);
  }
  /*
  DLOG(INFO) << "Portal distance in public graph";
  for (auto p1: dist_pub) {
    for (auto p2: dist_pub) {
      DLOG(INFO) << p1.first << "\t" << p2.first << "\t" << dist_pub[p1.first][p2.first];
    }
  }

  DLOG(INFO) << "Portal distance in private graph";
  for (auto p1: dist_pri) {
    for (auto p2: dist_pri) {
      DLOG(INFO) << p1.first << "\t" << p2.first << "\t" << dist_pri[p1.first][p2.first];
    }
  }

  LOG(INFO) << nodes_pub.size() << "\t" << nodes_pri.size();
   */


  /*
  for (auto n : nodes_pub) {
    DLOG(INFO) << "Klist";
    for (auto tmp : g_pub[n.second].klist) {
      DLOG(INFO) << tmp;
    }
    DLOG(INFO) << "PADS";
    for (auto tmp : g_pub[n.second].pads) {
      DLOG(INFO) << tmp.first << "\t" << tmp.second;
    }
  }
  for (int i = 1; i <= 7; i++) {
    DLOG(INFO) << "Keyword 2 vertices";
    for (auto vd :key2vertices_pub[i]) {
      DLOG(INFO) << vd;
    }
  }


  for(auto kwd: kpads.kpads){
    DLOG(INFO) << "Keywords\t" << kwd.second.size();
    DLOG(INFO) << kwd.first;
    for(auto wd: kwd.second){
      DLOG(INFO) << wd.first + 1 << "\t" << wd.second;
    }
  }
   */

  google::ShutdownGoogleLogging();
  google::ShutDownCommandLineFlags();
  return 0;
}
