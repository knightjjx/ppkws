#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <utility>
#include <algorithm>
#include <ctime>
#include <random>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/page_rank.hpp>

#include <glog/logging.h>
#include "io/Reader.hpp"


using namespace boost;

struct VertexP {
    // distance map: (w,d)
    std::unordered_map<int, int> pads;
    std::unordered_set<int> klist;
    int vid;
};

struct EdgeWeight {
    int weight;
};

typedef adjacency_list<vecS, vecS, directedS, VertexP, property<edge_weight_t, EdgeWeight>> Graph;
typedef property_map<Graph, vertex_index_t>::type IndexMap;
typedef iterator_property_map<std::vector<double>::iterator, IndexMap> RankMap;
typedef graph_traits<Graph>::vertex_iterator VIter;
typedef graph_traits<Graph>::out_edge_iterator EIter;
typedef graph_traits<Graph>::edge_iterator AEIter;
typedef graph_traits<Graph>::edge_descriptor edge_descriptor;
typedef std::pair<int, int> Edge;
typedef graph_traits<Graph>::vertex_descriptor VD;

struct KPADS {
    std::unordered_map<int, std::unordered_map<int, int>> kpads;
};

struct Blink {
    VD root;
    std::unordered_set<int> query;
    // Improve the refine performance
    // std::unordered_map<VD, int> d; // vertex -> distance

    // For completion
    std::unordered_map<int, VD> match; // keyword -> vertex
    std::unordered_map<int, int> dkeyword; // keyword -> distance

    int total_weight = 0;
};

using namespace std;

int k = 1;
KPADS kpads;

int npub;

std::unordered_map<int, VD> nodes_pub;
std::unordered_map<int, VD> nodes_pri;
std::unordered_set<VD> nodes_pri_vd;
std::unordered_map<int, std::unordered_map<int, int>> ads_labels;
std::unordered_map<int, std::unordered_set<VD>> key2vertices_pub;
std::unordered_map<int, std::unordered_set<VD>> key2vertices_pri;

void printLabels(std::unordered_map<int, std::unordered_map<int, int>> ads) {
  for (auto l: ads) {
    std::cout << l.first << ":\t";
    for (auto label: l.second) {
      std::cout << "(" << label.first << "," << label.second << ")" << "\t";
    }
    std::cout << std::endl;
  }
}

void loadADS(std::string adspath, Graph &g, std::unordered_map<int, VD> &nodes) {
  ifstream adsfile(adspath);
  std::string line;
  while (std::getline(adsfile, line)) {
    std::istringstream s(line);
    int vid, w, d;
    s >> vid;
    std::unordered_map<int, int> tmp;

    while ((s >> w >> d)) {
      tmp[w - 1] = d;
    }
    g[nodes[vid - 1]].pads = tmp;
  }

  /*
   * Generate KPADS by PADS
   */
  for (auto kv: key2vertices_pub) {
    auto keyword = kv.first;
    if (kpads.kpads.find(keyword) == kpads.kpads.end()) {
      std::unordered_map<int, int> ads;
      kpads.kpads.insert(std::make_pair(keyword, ads));
    }
    auto VDs = kv.second;

    std::unordered_map<int, int> toMerge;
    for (auto v: VDs) {
      for (auto wd: g[v].pads) {
        auto w = wd.first;
        auto d = wd.second;
        if (toMerge.find(w) == toMerge.end())
          toMerge.insert(std::make_pair(w, d));
        else {
          toMerge[w] = toMerge[w] > d ? d : toMerge[w];
        }
        /* Merge the common centers
        if (kpads.kpads[keyword].find(w) == kpads.kpads[keyword].end()) {
          kpads.kpads[keyword].insert(std::make_pair(w, d));
        } else {
          if (kpads.kpads[keyword][w] > d)
            kpads.kpads[keyword][w] = d;
        }
        */
      }
    }
    for (auto wd: toMerge) {
      auto w = wd.first;
      auto d = wd.second;
      kpads.kpads[keyword].insert(std::make_pair(w, d));
    }
  }
}

std::unordered_map<int, int> loadPortal(std::string path_portal) {
  ifstream portalfile(path_portal + ".p");
  std::unordered_map<int, int> portals;
  int n;
  portalfile >> n;

  for (int i = 0; i < n; i++) {
    int pub, pri;
    portalfile >> pub >> pri;
    portals[pub - 1] = pri - 1;
  }
  return portals;
}

std::unordered_map<VD, std::unordered_map<VD, int>> dist;
std::unordered_map<VD, std::unordered_set<VD>> nindex;

struct KV{
    VD current;
    VD origin;
};

vector<Blink> PPBlinks(Graph &g, std::unordered_map<int, std::unordered_set<VD>> &key2vertices,
                       std::unordered_set<int> query, int R=2) {
  std::unordered_map<int, std::vector<VD>> origins;


  vector<Blink> ans;
  std::unordered_map<int, Blink> allans;

  /*
   * Init the search origin and Append the portal sets
   */
  for (auto key: query) {
    std::vector<VD> key2vertices_V(key2vertices[key].begin(), key2vertices[key].end());

    int hop = 0;
    std::queue<KV> q, next_hop;

    for(auto ele: key2vertices_V){
      KV e;
      e.origin = ele;
      e.current = ele;
      q.push(e);
    }


    std::unordered_set<VD> visited;

    while (hop < R) {
      while (!q.empty()) {
        KV head = q.front();
        q.pop();
        if(visited.find(head.current)!=visited.end())
          continue;
        visited.insert(head.current);

        Blink a;
        if(allans.find(head.current) == allans.end()){
          a.root = head.current;
          a.match[key] = head.origin;
          a.dkeyword[key] = hop;
          a.query = query;
          allans.insert(std::make_pair(head.current, a));
        } else{
          a = allans[head.current];
          if(a.dkeyword.find(key) == a.dkeyword.end() || a.dkeyword[key] > hop){
            allans[head.current].match[key] = head.origin;
            allans[head.current].dkeyword[key] = hop;
          }
        }

        for (std::pair<EIter, EIter> e = out_edges(head.current, g); e.first != e.second; e.first++) {
          VD t = target(*e.first, g);
          if(visited.find(t) != visited.end())
            continue;
          KV te;
          te.current = t;
          te.origin = head.origin;
          next_hop.push(te);
        }
      }
      q = next_hop;
      hop++;
      if (q.empty()) {
        break;
      }
    }
  }

  for(auto& a: allans){
    bool flag = false;
    auto ablinks = a.second;
    for(auto m: ablinks.match){
      if(nodes_pri_vd.find(m.second)!=nodes_pri_vd.end())
        flag = true;
    }
    if(ablinks.match.size() < ablinks.query.size())
      flag = false;
    if(flag)
      ans.push_back(a.second);
  }
  return ans;
}

Graph loadGraph(std::string basepath, std::unordered_map<int, VD> &nodes, std::unordered_map<int, VD> &nodes_pri,
                std::unordered_map<int, std::unordered_set<VD>> &key2vertices, std::string basepath_pri,
                std::unordered_map<int, int> &portals) {
  Graph g;
  /**
   * Load Vertices
   */
//  ifstream vfile("/Users/samjjx/dev/git/ppkws/cpp/data/sample.v");
  ifstream vfile(basepath + ".v");
  int nv;
  vfile >> nv;


  for (int i = 0; i < nv; i++) {
    int vid, nk;
    vfile >> vid;
    vfile >> nk;

    std::unordered_set<int> klist;

    nodes[i] = add_vertex(g);

    for (int j = 0; j < nk; j++) {
      int k;
      vfile >> k;
      klist.insert(k);
      if (key2vertices.find(k) == key2vertices.end()) {
        std::unordered_set<VD> tmp;
        key2vertices.insert(make_pair(k, tmp));
      }
      key2vertices[k].insert(nodes[i]);
    }
    g[nodes[i]].klist = klist;
  }
  LOG(INFO) << num_vertices(g) << std::endl;
  LOG(INFO) << "Finish loading public vertices";
  /**
   * Load Edges
   */
  ifstream efile(basepath + ".e");
  int m;
  efile >> m;
  int a, b, weight, i = 0;

  EdgeWeight wedge;
  wedge.weight = 1;

  while (efile >> a && efile >> b && efile >> weight) {
    add_edge(nodes[a - 1], nodes[b - 1], wedge, g);
    add_edge(nodes[b - 1], nodes[a - 1], wedge, g);
  }

  LOG(INFO) << num_edges(g) << std::endl;
  LOG(INFO) << "Finish loading public edges" << std::endl;


  /**
   * Load Private Vertices
   */
//  ifstream vfile("/Users/samjjx/dev/git/ppkws/cpp/data/sample.v");
  ifstream vfile_pri(basepath_pri + ".v");
  int nv_pri;
  vfile_pri >> nv_pri;

  std::unordered_map<int, int> privid2pub;

  for (int i = 0; i < nv_pri; i++) {
    int vid, nk;
    vfile_pri >> vid;
    vfile_pri >> nk;
    std::unordered_set<int> klist;

    for (int j = 0; j < nk; j++) {
      int k;
      vfile_pri >> k;
      klist.insert(k);
    }
    if (portals.find(vid - 1) != portals.end()) {
      privid2pub.insert(std::make_pair(vid, portals[vid - 1]));
      nodes_pri_vd.insert(portals[vid - 1]);
      for (auto k: klist) {
        if (key2vertices.find(k) == key2vertices.end()) {
          std::unordered_set<VD> tmp;
          key2vertices.insert(make_pair(k, tmp));
        }
        key2vertices[k].insert(portals[vid - 1]);
      }
      continue;
    }

    nodes[i + nv] = add_vertex(g);
    nodes_pri[i] = nodes[i + nv];
    privid2pub.insert(std::make_pair(vid, nodes[i + nv]));
    nodes_pri_vd.insert(nodes[i + nv]);

    for (auto k: klist) {
      if (key2vertices.find(k) == key2vertices.end()) {
        std::unordered_set<VD> tmp;
        key2vertices.insert(make_pair(k, tmp));
      }
      key2vertices[k].insert(nodes_pri[i]);
    }
    g[nodes[i+nv]].klist = klist;
  }
  LOG(INFO) << num_vertices(g);
  LOG(INFO) << "Finish loading private vertices";
  LOG(INFO) << "Private nodes\t" << nodes_pri_vd.size() << std::endl;

  /**
   * Load Edges
   */
  ifstream efile_pri(basepath_pri + ".e");
  int m_pri;
  efile_pri >> m_pri;

  wedge.weight = 1;

  while (efile_pri >> a && efile_pri >> b && efile_pri >> weight) {
    add_edge(privid2pub[a], privid2pub[b], wedge, g);
    add_edge(privid2pub[b], privid2pub[a], wedge, g);
  }

  LOG(INFO) << num_edges(g) << std::endl;
  LOG(INFO) << "Finish loading public edges" << std::endl;

  npub = nv;
  return g;
}


void print_stat() {
  int cnt = 0;
  for (auto vid: ads_labels) {
    cnt += vid.second.size();
  }
  std::cout << "Labels Size=\t" << cnt << std::endl;
}

void printRclique(Blink a, Graph &g) {
  LOG(INFO) << "**************************";
  LOG(INFO) << "Source \t " << a.root << "\t" << g[a.root].klist.size();
  for (auto vd: a.dkeyword) {
    LOG(INFO) << "Keyword\t" << vd.first << "\t dist\t " << vd.second;
  }
  for (auto kv: a.match) {
    LOG(INFO) << "Keyword\t" << kv.first << "\t Vertex\t " << kv.second;
  }
  LOG(INFO) << "**************************";
}

void loadQuery(std::string basepath, std::unordered_set<int> &query) {
  ifstream qfile(basepath);
  if(!qfile.is_open()){
    return;
  }
  int nq;
  qfile >> nq;

  for (int i = 0; i < nq; i++) {
    int q;
    qfile >> q;
    query.insert(q);
  }
}

void loadQueries(std::string basepath, std::vector<std::unordered_set<int>> &queries) {
  for (int i = 0; i < 20; i++) {
    std::unordered_set<int> query;
    try {
      loadQuery(basepath + to_string(i + 1) + ".ptn", query);
    } catch (const char *msg) {
      continue;
    }
    if(query.size() == 0)
      continue;
    queries.push_back(query);
  }
  LOG(INFO) << "Finish loading query";
}

void print_graph(Graph &g) {
  AEIter i, end;
  for (tie(i, end) = edges(g); i != end; i++) {
    auto v = target(*i, g);
    auto u = source(*i, g);
    if (v > u)
      continue;
    LOG(INFO) << v + 1 << "\t" << u + 1;
  }
}

std::unordered_set<VD> generate_index_set(std::vector<std::unordered_set<int>> queries,
                                          std::unordered_map<int, std::unordered_set<VD>> &key2vertices) {
  std::unordered_set<VD> allVD;
  for (auto q: queries) {
    for (auto k: q) {
      for (auto v: key2vertices[k]) {
        allVD.insert(v);
      }
    }
  }
  return allVD;
}

int main(int argc, char *argv[]) {
  FLAGS_alsologtostderr = 1;
  FLAGS_colorlogtostderr = 1;
  // 解析命令行参数
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  // 初始化日志库
  google::InitGoogleLogging(argv[0]);

  if (argc <= 3) {
    LOG(INFO) << "You must provide at least two argument\n";
    exit(0);
  }
  std::string path_pub = argv[1];
  std::string path_pri = argv[2];
  std::string path_portal = argv[3];

  std::unordered_map<int, int> portals = loadPortal(path_portal);

  Graph g_c = loadGraph(path_pub, nodes_pub, nodes_pri, key2vertices_pub, path_pri, portals);
//  print_graph(g_c);


  std::vector<std::unordered_set<int>> queries;
  loadQueries(argv[4], queries);

  FLAGS_log_dir = argv[5];

  LOG(INFO) << num_edges(g_c) << std::endl;
  LOG(INFO) << num_vertices(g_c) << std::endl;



  /*
   * Partial Evaluation
   */
  for (int i = 0; i < queries.size(); i++) {
    auto query = queries[i];
    LOG(INFO) << "Pattern\t#" << (i + 1);
    std::clock_t start;
    double duration;
    start = std::clock();
    auto ans = PPBlinks(g_c, key2vertices_pub, query, 3);
    duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
    LOG(INFO) << "Size of ans after evaluation " << ans.size();
    LOG(INFO) << "Evaluation time \t" << duration;

    for (auto a: ans) {
      printRclique(a, g_c);
    }
  }
  /*
  DLOG(INFO) << "Portal distance in public graph";
  for (auto p1: dist_pub) {
    for (auto p2: dist_pub) {
      DLOG(INFO) << p1.first << "\t" << p2.first << "\t" << dist_pub[p1.first][p2.first];
    }
  }

  DLOG(INFO) << "Portal distance in private graph";
  for (auto p1: dist_pri) {
    for (auto p2: dist_pri) {
      DLOG(INFO) << p1.first << "\t" << p2.first << "\t" << dist_pri[p1.first][p2.first];
    }
  }

  LOG(INFO) << nodes_pub.size() << "\t" << nodes_pri.size();
   */


  /*
  for (auto n : nodes_pub) {
    DLOG(INFO) << "Klist";
    for (auto tmp : g_pub[n.second].klist) {
      DLOG(INFO) << tmp;
    }
    DLOG(INFO) << "PADS";
    for (auto tmp : g_pub[n.second].pads) {
      DLOG(INFO) << tmp.first << "\t" << tmp.second;
    }
  }
  for (int i = 1; i <= 7; i++) {
    DLOG(INFO) << "Keyword 2 vertices";
    for (auto vd :key2vertices_pub[i]) {
      DLOG(INFO) << vd;
    }
  }


  for(auto kwd: kpads.kpads){
    DLOG(INFO) << "Keywords\t" << kwd.second.size();
    DLOG(INFO) << kwd.first;
    for(auto wd: kwd.second){
      DLOG(INFO) << wd.first + 1 << "\t" << wd.second;
    }
  }
   */

  google::ShutdownGoogleLogging();
  google::ShutDownCommandLineFlags();
  return 0;
}
