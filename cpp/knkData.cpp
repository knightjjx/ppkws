#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <utility>
#include <algorithm>
#include <ctime>
#include <random>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/page_rank.hpp>

#include <glog/logging.h>
#include "io/Reader.hpp"


using namespace boost;

struct VertexP {
    // distance map: (w,d)
    std::unordered_map<int, int> pads;
    std::unordered_set<int> klist;
    int vid;
};

struct EdgeWeight {
    int weight;
};

typedef adjacency_list<vecS, vecS, directedS, VertexP, property<edge_weight_t, EdgeWeight>> Graph;
typedef property_map<Graph, vertex_index_t>::type IndexMap;
typedef iterator_property_map<std::vector<double>::iterator, IndexMap> RankMap;
typedef graph_traits<Graph>::vertex_iterator VIter;
typedef graph_traits<Graph>::out_edge_iterator EIter;
typedef graph_traits<Graph>::edge_iterator AEIter;
typedef graph_traits<Graph>::edge_descriptor edge_descriptor;
typedef std::pair<int, int> Edge;
typedef graph_traits<Graph>::vertex_descriptor VD;

struct KPADS {
    std::unordered_map<int, std::unordered_map<int, int>> kpads;
};



using namespace std;

int k = 1;
KPADS kpads;

int npub;

std::unordered_map<int, VD> nodes_pub;
std::unordered_map<int, VD> nodes_pri;
std::unordered_set<VD> nodes_pri_vd;
std::unordered_map<int, std::unordered_map<int, int>> ads_labels;
std::unordered_map<int, std::unordered_set<VD>> key2vertices_pub;
std::unordered_map<int, std::unordered_set<VD>> key2vertices_pri;

void printLabels(std::unordered_map<int, std::unordered_map<int, int>> ads) {
  for (auto l: ads) {
    std::cout << l.first << ":\t";
    for (auto label: l.second) {
      std::cout << "(" << label.first << "," << label.second << ")" << "\t";
    }
    std::cout << std::endl;
  }
}

void loadADS(std::string adspath, Graph &g, std::unordered_map<int, VD> &nodes) {
  ifstream adsfile(adspath);
  std::string line;
  while (std::getline(adsfile, line)) {
    std::istringstream s(line);
    int vid, w, d;
    s >> vid;
    std::unordered_map<int, int> tmp;

    while ((s >> w >> d)) {
      tmp[w - 1] = d;
    }
    g[nodes[vid - 1]].pads = tmp;
  }

  /*
   * Generate KPADS by PADS
   */
  for (auto kv: key2vertices_pub) {
    auto keyword = kv.first;
    if (kpads.kpads.find(keyword) == kpads.kpads.end()) {
      std::unordered_map<int, int> ads;
      kpads.kpads.insert(std::make_pair(keyword, ads));
    }
    auto VDs = kv.second;

    std::unordered_map<int, int> toMerge;
    for (auto v: VDs) {
      for (auto wd: g[v].pads) {
        auto w = wd.first;
        auto d = wd.second;
        if (toMerge.find(w) == toMerge.end())
          toMerge.insert(std::make_pair(w, d));
        else {
          toMerge[w] = toMerge[w] > d ? d : toMerge[w];
        }
        /* Merge the common centers
        if (kpads.kpads[keyword].find(w) == kpads.kpads[keyword].end()) {
          kpads.kpads[keyword].insert(std::make_pair(w, d));
        } else {
          if (kpads.kpads[keyword][w] > d)
            kpads.kpads[keyword][w] = d;
        }
        */
      }
    }
    for (auto wd: toMerge) {
      auto w = wd.first;
      auto d = wd.second;
      kpads.kpads[keyword].insert(std::make_pair(w, d));
    }
  }
}

std::unordered_map<int, int> loadPortal(std::string path_portal) {
  ifstream portalfile(path_portal + ".p");
  std::unordered_map<int, int> portals;
  int n;
  portalfile >> n;

  for (int i = 0; i < n; i++) {
    int pub, pri;
    portalfile >> pub >> pri;
    portals[pub - 1] = pri - 1;
  }
  return portals;
}

std::unordered_map<VD, std::unordered_map<VD, int>> dist;
std::unordered_map<VD, std::unordered_set<VD>> nindex;

struct KV{
    VD current;
    VD origin;
};

template<typename S>
auto select_random(const S &s, size_t n) {
  auto it = std::begin(s);
  // 'advance' the iterator n times
  std::advance(it,n);
  return it;
}

Graph loadGraph(std::string basepath, std::unordered_map<int, VD> &nodes, std::unordered_map<int, VD> &nodes_pri,
                std::unordered_map<int, std::unordered_set<VD>> &key2vertices, std::string basepath_pri,
                std::unordered_map<int, int> &portals) {
  Graph g;
  /**
   * Load Vertices
   */
//  ifstream vfile("/Users/samjjx/dev/git/ppkws/cpp/data/sample.v");

  std::unordered_set<int> pubkey, prikey;
  ifstream vfile(basepath + ".v");
  int nv;
  vfile >> nv;


  for (int i = 0; i < nv; i++) {
    int vid, nk;
    vfile >> vid;
    vfile >> nk;

    std::unordered_set<int> klist;

    nodes[i] = add_vertex(g);

    for (int j = 0; j < nk; j++) {
      int k;
      vfile >> k;
      pubkey.insert(k);
      klist.insert(k);
      if (key2vertices.find(k) == key2vertices.end()) {
        std::unordered_set<VD> tmp;
        key2vertices.insert(make_pair(k, tmp));
      }
      key2vertices[k].insert(nodes[i]);
    }
    g[nodes[i]].klist = klist;
  }
  LOG(INFO) << num_vertices(g) << std::endl;
  LOG(INFO) << "Finish loading public vertices";
  /**
   * Load Edges
   */
  ifstream efile(basepath + ".e");
  int m;
  efile >> m;
  int a, b, weight, i = 0;

  EdgeWeight wedge;
  wedge.weight = 1;

  while (efile >> a && efile >> b && efile >> weight) {
    add_edge(nodes[a - 1], nodes[b - 1], wedge, g);
    add_edge(nodes[b - 1], nodes[a - 1], wedge, g);
  }

  LOG(INFO) << num_edges(g) << std::endl;
  LOG(INFO) << "Finish loading public edges" << std::endl;


  /**
   * Load Private Vertices
   */
//  ifstream vfile("/Users/samjjx/dev/git/ppkws/cpp/data/sample.v");
  ifstream vfile_pri(basepath_pri + ".v");
  int nv_pri;
  vfile_pri >> nv_pri;

  std::unordered_map<int, int> privid2pub;

  /**
   * Private structure for knk
   */
  std::vector<int> edges_knk_pri_src;
  std::vector<int> edges_knk_pri_dst;
  std::unordered_map<int, std::unordered_set<int>> vertices_knk_pri;

  /**
   * END
   */

  for (int i = 0; i < nv_pri; i++) {
    int vid, nk;
    vfile_pri >> vid;
    vfile_pri >> nk;
    std::unordered_set<int> klist;

    for (int j = 0; j < nk; j++) {
      int k;
      vfile_pri >> k;
      prikey.insert(k);
      klist.insert(k);
    }

    vertices_knk_pri.insert(std::make_pair(vid, klist));

    if (portals.find(vid - 1) != portals.end()) {
      privid2pub.insert(std::make_pair(vid, portals[vid - 1]));
      nodes_pri_vd.insert(portals[vid - 1]);
      for (auto k: klist) {
        if (key2vertices.find(k) == key2vertices.end()) {
          std::unordered_set<VD> tmp;
          key2vertices.insert(make_pair(k, tmp));
        }
        key2vertices[k].insert(portals[vid - 1]);
      }
      continue;
    }

    nodes[i + nv] = add_vertex(g);
    nodes_pri[i] = nodes[i + nv];
    privid2pub.insert(std::make_pair(vid, nodes[i + nv]));
    nodes_pri_vd.insert(nodes[i + nv]);

    for (auto k: klist) {
      if (key2vertices.find(k) == key2vertices.end()) {
        std::unordered_set<VD> tmp;
        key2vertices.insert(make_pair(k, tmp));
      }
      key2vertices[k].insert(nodes_pri[i]);
    }
    g[nodes[i+nv]].klist = klist;
  }
  LOG(INFO) << num_vertices(g);
  LOG(INFO) << "Finish loading private vertices";
  LOG(INFO) << "Private nodes\t" << nodes_pri_vd.size() << std::endl;

  /**
   * Load Edges
   */
  ifstream efile_pri(basepath_pri + ".e");
  int m_pri;
  efile_pri >> m_pri;

  wedge.weight = 1;

  while (efile_pri >> a && efile_pri >> b && efile_pri >> weight) {
    add_edge(privid2pub[a], privid2pub[b], wedge, g);
    add_edge(privid2pub[b], privid2pub[a], wedge, g);
    edges_knk_pri_src.push_back(a-1);
    edges_knk_pri_dst.push_back(b-1);
  }

  LOG(INFO) << num_edges(g) << std::endl;
  LOG(INFO) << "Finish loading priate edges" << std::endl;

  npub = nv;

  /**
   * Generate knk experiment data
   */
  ofstream myfile;
  ofstream myfileppquery;

  myfile.open(basepath + ".graph");
  myfile << num_vertices(g) << "\n";

  AEIter start, end;
  for (tie(start, end) = edges(g); start != end; start++) {
    auto v = target(*start, g);
    auto u = source(*start, g);
    if (v > u)
      continue;

    myfile << v << " " << u << " " << 1 << "\n";
    myfile << u << " " << v << " " << 1 << "\n";
  }

  myfile.flush();
  myfile.close();

  myfileppquery.open(basepath + ".pri.graph");
  myfileppquery <<  vertices_knk_pri.size() << "\n";

  for(int i = 0; i < edges_knk_pri_src.size(); i++){
    myfileppquery << edges_knk_pri_src[i] << " " <<  edges_knk_pri_dst[i] << " 1\n";
    myfileppquery << edges_knk_pri_dst[i] << " " <<  edges_knk_pri_src[i] << " 1\n";
  }
  myfileppquery.flush();
  myfileppquery.close();


  myfile.open(basepath + ".graph.keyNode");

  int cnt = 0;
  for(auto kvs: key2vertices){
    cnt += kvs.second.size();
  }

  myfile << key2vertices.size() << " " << cnt << "\n";
  for(int i = 0; i < key2vertices.size(); i++){
    auto vds = key2vertices[i+1];
    myfile << i << " " << vds.size();
    for(auto vd: vds){
      myfile << " " << vd;
    }
    myfile << "\n";
  }

  myfile.flush();
  myfile.close();


  myfileppquery.open(basepath + ".pri.graph.keyNode");
  cnt = 0;
  for(auto kvs: key2vertices_pri){
    cnt += kvs.second.size();
  }
  myfileppquery << key2vertices.size() << "\t" << cnt << "\n";

  std::unordered_map<int, std::unordered_set<VD>> key2vertices_pri;

  for(auto vk: vertices_knk_pri){
    auto vid =  vk.first;
    auto vklist =  vk.second;
    for(auto k: vklist){
      key2vertices_pri[k - 1].insert(vid);
    }
  }
  for(int i = 0; i < key2vertices.size(); i++){
    if(key2vertices_pri.find(i) == key2vertices_pri.end()){
      myfileppquery << i << " 0\n";
      continue;
    }
    myfileppquery << i << " " << key2vertices_pri[i].size();
    for(auto v: key2vertices_pri[i]){
      myfileppquery << " " << v;
    }
    myfileppquery << "\n";
  }
  myfileppquery.flush();
  myfileppquery.close();

  myfile.open(basepath + ".graph.nodeKey");
  cnt = 0;
  for(VD n =0; n < num_vertices(g); n++){
    cnt += g[n].klist.size();
  }
  myfile << key2vertices.size() << " " << cnt << "\n";
  for(VD n =0; n < num_vertices(g); n++){
    myfile << n  << " " << g[n].klist.size();
    for(auto k: g[n].klist){
      myfile << " " << k;
    }
    myfile << "\n";
  }

  myfile.flush();
  myfile.close();


  myfileppquery.open(basepath + ".pri.graph.nodeKey");
  cnt = 0;
  for(auto vk: vertices_knk_pri) {
    cnt += vk.second.size();
  }
  myfileppquery << key2vertices.size() << " " << cnt << "\n";

  for(auto vk: vertices_knk_pri) {
    auto vid = vk.first;
    auto vklist = vk.second;
    myfileppquery << vid - 1  << " " << vklist.size();
    for(auto k: vklist){
      myfileppquery << " " << k - 1;
    }

    myfileppquery << "\n";

  }

  myfileppquery.flush();
  myfileppquery.close();


  /**
   * Generate query: keywords in private only, keywords in public only
   */

  myfile.open(basepath + ".graph.testcase");

  myfileppquery.open(basepath + ".pri.graph.testcase");
  myfileppquery  << "20\n";

  myfile << "20\n";

  for(int i = 0; i < 10; i++){
    auto r = rand() % prikey.size();
    auto x =  privid2pub[r % privid2pub.size()+1];
    int qkeyword = *select_random(prikey, r);
    myfile << x << "\t" << qkeyword - 1  << "\t0\t0\n";
    myfileppquery  << r % privid2pub.size() << "\t" << qkeyword - 1  << "\t0\t0\n";
  }
  for(int i = 10; i < 20; i++){
    auto r = rand() % pubkey.size();
    auto x =  privid2pub[r % privid2pub.size()+1];
    int qkeyword = *select_random(pubkey, r);
    myfile << x << "\t" << qkeyword - 1  << "\t0\t0\n";
    myfileppquery  << r % privid2pub.size() << "\t" << qkeyword - 1  << "\t0\t0\n";
  }

  myfile.flush();
  myfile.close();

  myfileppquery.flush();
  myfileppquery.close();

  return g;
}


void print_stat() {
  int cnt = 0;
  for (auto vid: ads_labels) {
    cnt += vid.second.size();
  }
  std::cout << "Labels Size=\t" << cnt << std::endl;
}




void print_graph(Graph &g) {
  AEIter i, end;
  for (tie(i, end) = edges(g); i != end; i++) {
    auto v = target(*i, g);
    auto u = source(*i, g);
    if (v > u)
      continue;
    LOG(INFO) << v + 1 << "\t" << u + 1;
  }
}



int main(int argc, char *argv[]) {
  FLAGS_alsologtostderr = 1;
  FLAGS_colorlogtostderr = 1;
  // 解析命令行参数
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  // 初始化日志库
  google::InitGoogleLogging(argv[0]);

  if (argc <= 3) {
    LOG(INFO) << "You must provide at least two argument\n";
    exit(0);
  }
  std::string path_pub = argv[1];
  std::string path_pri = argv[2];
  std::string path_portal = argv[3];

  std::unordered_map<int, int> portals = loadPortal(path_portal);

  Graph g_c = loadGraph(path_pub, nodes_pub, nodes_pri, key2vertices_pub, path_pri, portals);

  std::vector<std::unordered_set<int>> queries;

  LOG(INFO) << num_edges(g_c) << std::endl;
  LOG(INFO) << num_vertices(g_c) << std::endl;

  google::ShutdownGoogleLogging();
  google::ShutDownCommandLineFlags();
  return 0;
}
